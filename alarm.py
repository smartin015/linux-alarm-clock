import os
from datetime import datetime
import time
import sys

DELAY = 10
AUDIO_FILE = "sounds/dialup.wav"

def toSec(hour, minute):
	return (24+hour + minute/60.0)*3600

(hour,minute) = sys.argv[1].split(":")
now = datetime.now()

if now.hour > 12:
	hour = int(hour)+24

seconds = int(toSec(int(hour), int(minute)) - toSec(now.hour, now.minute) - now.second)


sleep = "sudo rtcwake -m mem -s "+str(seconds)
delay = "sleep "+str(DELAY)
setvolume = "pacmd set-sink-volume 0 65535"
playwav = "aplay ~/dev/alarm/"+AUDIO_FILE

cmd = " && ".join([sleep, delay, setvolume, playwav, playwav])
print cmd
os.system(cmd)
